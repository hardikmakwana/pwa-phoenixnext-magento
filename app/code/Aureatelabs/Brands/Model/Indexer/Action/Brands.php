<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Brands
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Brands\Model\Indexer\Action;

use Aureatelabs\Brands\Model\ResourceModel\Brand as BrandResource;
use Magento\Framework\App\Area;
use Magento\Framework\App\AreaList;

/**
 * Class Brands
 * @package Aureatelabs\Brands\Model\Indexer\Action
 */
class Brands
{
    /**
     * @var AreaList
     */
    private $areaList;

    /**
     * @var BrandResource
     */
    private $resourceModel;

    /**
     * @var \Aureatelabs\Brands\Model\ImageUploader
     */
    protected $imageUploader;

    /**
     * Brands constructor.
     *
     * @param AreaList $areaList
     * @param BrandResource $brandResource
     * @param \Aureatelabs\Brands\Model\ImageUploader $imageUploader
     * @param \Magento\Framework\Serialize\Serializer\Json $json
     */
    public function __construct(
        AreaList $areaList,
        BrandResource $brandResource,
        \Aureatelabs\Brands\Model\ImageUploader $imageUploader
    ) {
        $this->areaList = $areaList;
        $this->resourceModel = $brandResource;
        $this->imageUploader = $imageUploader;
    }

    /**
     * @param int $storeId
     * @param array $brandsIds
     *
     * @return \Traversable
     * @throws \Exception
     */
    public function rebuild($storeId = 1, array $brandsIds = [])
    {
        $this->areaList->getArea(Area::AREA_FRONTEND)->load(Area::PART_DESIGN);
        $brandss = $this->resourceModel->loadBrands($storeId, $brandsIds);
        foreach ($brandss as $brands) {

            $lastBrandsId = $brands['brand_id'];
            $brands['id'] = $brands['brand_id'];

            $brands['image'] = $this->imageUploader->getFileUrl($brands['brand_image']);

            yield $lastBrandsId => $brands;
        }
    }
}
