<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Brands
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Brands\Model\Indexer;

use Divante\VsbridgeIndexerCore\Indexer\StoreManager;
use Aureatelabs\Brands\Model\Indexer\Action\Brands as BrandsAction;
use Divante\VsbridgeIndexerCore\Indexer\GenericIndexerHandler;

/**
 * Class Brands
 * @package Aureatelabs\Brands\Model\Indexer
 */
class Brands implements \Magento\Framework\Indexer\ActionInterface, \Magento\Framework\Mview\ActionInterface
{
    /**
     * @var GenericIndexerHandler
     */
    private $indexHandler;

    /**
     * @var BrandsAction
     */
    private $brandsAction;

    /**
     * @var StoreManager
     */
    private $storeManager;

    /**
     * Brands constructor.
     *
     * @param GenericIndexerHandler $indexerHandler
     * @param StoreManager $storeManager
     * @param BrandsAction $action
     */
    public function __construct(
        GenericIndexerHandler $indexerHandler,
        StoreManager $storeManager,
        BrandsAction $action
    ) {
        $this->indexHandler = $indexerHandler;
        $this->storeManager = $storeManager;
        $this->brandsAction = $action;
    }

    /*
     * Used by mview, allows process indexer in the "Update on schedule" mode
     * Used by mview, allows you to process multiple placed orders in the "Update on schedule" mode
     */
    public function execute($ids)
    {
        $stores = $this->storeManager->getStores();

        foreach ($stores as $store) {
            $this->indexHandler->saveIndex($this->brandsAction->rebuild($store->getId(), $ids), $store);
            $this->indexHandler->cleanUpByTransactionKey($store, $ids);
        }
    }

    /*
     * Will take all of the data and reindex
     * Will run when reindex via command line
     * Should take into account all placed orders in the system
     */
    public function executeFull()
    {
        $stores = $this->storeManager->getStores();

        foreach ($stores as $store) {
            $this->indexHandler->saveIndex($this->brandsAction->rebuild($store->getId()), $store);
            $this->indexHandler->cleanUpByTransactionKey($store);
        }
    }

    /*
     * Works with a set of entity changed (may be massaction)
     * Works with a set of placed orders (mass actions and so on)
     */
    public function executeList(array $ids)
    {
        $this->execute($ids);
    }

    /*
     * Works in runtime for a single entity using plugins
     * Works in runtime for a single order using plugins
     */
    public function executeRow($id)
    {
        $this->execute([$id]);
    }
}
