<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\RewardsPoint\Model;

class GetRewardsHistory implements \Aureatelabs\RewardsPoint\Api\GetRewardsHistoryInterface
{

    /**
     * @var \Mirasvit\Rewards\Model\TransactionFactory
     */
    protected $transactionFactory;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    private $customerRepository;

    /**
     * @param \Mirasvit\Rewards\Model\TransactionFactory $transactionFactory
     * @param \Magento\Customer\Model\Customer           $customerRepository
     */
    public function __construct(
        \Mirasvit\Rewards\Model\TransactionFactory $transactionFactory,
        \Magento\Customer\Model\Customer $customerRepository
    ) {
        $this->transactionFactory = $transactionFactory;
        $this->customerRepository = $customerRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getRewardsHistory($customer_id)
    {
        $collection = $this->transactionFactory->create()
            ->getCollection()
            ->addFieldToFilter('customer_id', $customer_id);

            $res = [];
            foreach ($collection as $rewardshistories) {
               $res[] = $rewardshistories->getData();
            }

        return $res;
    }
}

