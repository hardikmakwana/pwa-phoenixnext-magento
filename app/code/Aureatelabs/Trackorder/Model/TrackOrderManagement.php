<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Trackorder
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Trackorder\Model;

use Magento\Sales\Model\Order;

/**
 * Class TrackOrderManagement
 * @package Aureatelabs\Trackorder\Model
 */
class TrackOrderManagement implements \Aureatelabs\Trackorder\Api\TrackOrderManagementInterface
{
    /**
     * @var \Aureatelabs\Trackorder\Api\TrackOrderResultInterface
     */
    private $_trackOrderResult;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */
    private $_order;

    /**
     * @var \Magento\Quote\Model\MaskedQuoteIdToQuoteIdInterface
     */
    private $maskedQuoteIdToQuoteId;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Region\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    protected $_invoiceService;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    protected $_transaction;

    /**
     * TrackOrderManagement constructor.
     * @param \Aureatelabs\Trackorder\Api\TrackOrderResultInterface $trackOrderResult
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Quote\Model\MaskedQuoteIdToQuoteIdInterface $maskedQuoteIdToQuoteId
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $collectionFactory
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Sales\Model\Service\InvoiceService $invoiceService
     * @param \Magento\Framework\DB\Transaction $transaction
     */
    function __construct(
        \Aureatelabs\Trackorder\Api\TrackOrderResultInterface $trackOrderResult,
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Quote\Model\MaskedQuoteIdToQuoteIdInterface $maskedQuoteIdToQuoteId,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $collectionFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $transaction
    ) {

        $this->_trackOrderResult = $trackOrderResult;
        $this->_order = $order;
        $this->quoteRepository = $quoteRepository;
        $this->maskedQuoteIdToQuoteId = $maskedQuoteIdToQuoteId;
        $this->collectionFactory = $collectionFactory;
        $this->stockRegistry = $stockRegistry;
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $transaction;
    }

    /**
     * Get order track data.
     *
     * @param \Aureatelabs\Trackorder\Api\TrackOrderInterface $trackOrder
     * @return \Aureatelabs\Trackorder\Api\TrackOrderResultInterface $trackOrderResult
     */
    public function track(\Aureatelabs\Trackorder\Api\TrackOrderInterface $trackOrder)
    {
        $result = [];
        $this->_trackOrderResult->setMessage('Error');

        $cartId = trim($trackOrder->getCartId());
        $status = trim($trackOrder->getStatus());

        if(!empty($cartId) && !empty($status)) {
            try {
                $quoteId = $this->getQuoteIdByMask($cartId);
            } catch (\Exception $e) {
                $quoteId = $cartId;
            }

            $order = $this->_order->load($cartId, 'quote_id');
            if(strtolower($status) === 'success') {
                if($order->canInvoice()) {
                    $invoice = $this->_invoiceService->prepareInvoice($order);
                    $invoice->register();
                    $invoice->save();

                    $transactionSave = $this->_transaction->addObject($invoice)->addObject($invoice->getOrder());
                    $transactionSave->save();

                    $order->setState(Order::STATE_PROCESSING)->setStatus(Order::STATE_PROCESSING);
                    $order->save();
                }
            } else {
                $order->setState(Order::STATE_CANCELED)->setStatus(Order::STATE_CANCELED);
                $order->save();
            }
        } else {
            $this->_trackOrderResult->setCode(400);
            $result = 'Invalid value for cart id and/or order status';
        }

        $this->_trackOrderResult->setCode(200);
        $this->_trackOrderResult->setMessage('Success');
        $result = "We found tracking information for cart #$cartId";

        $this->_trackOrderResult->setResult($result);
        return $this->_trackOrderResult;
    }

    /**
     * @param string $mask
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getQuoteIdByMask(string $mask)
    {
        return $this->maskedQuoteIdToQuoteId->execute($mask);
    }
}
