<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Contact
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Contact\Api\Data;

/**
 * Interface SubscriberInterface
 * @api
 * @package Aureatelabs\Contact\Api\Data
 */
interface SubscriberInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const NAME      = 'name';
    const EMAIL     = 'email';
    const TELEPHONE = 'telephone';
    const COMMENT   = 'comment';
    /**#@-*/

    /**
     * Get Name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set Name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * Get Email
     *
     * @return string|null
     */
    public function getEmail();

    /**
     * Set Email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email);

    /**
     * Get Telephone
     *
     * @return string|null
     */
    public function getTelephone();

    /**
     * Set Telephone
     *
     * @param string $telephone
     * @return $this
     */
    public function setTelephone($telephone);

    /**
     * Get Comment
     *
     * @return string|null
     */
    public function getComment();

    /**
     * Set Comment
     *
     * @param string $comment
     * @return $this
     */
    public function setComment($comment);
}
