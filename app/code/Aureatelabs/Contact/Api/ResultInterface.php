<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Contact
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Contact\Api;

/**
 * Result interface.
 * @api
 */
interface ResultInterface
{
    /**
     * Get response code.
     *
     * @return integer/null
     */
    public function getCode();

    /**
     * Set response code.
     *
     * @param integer $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get response message.
     *
     * @return string|null
     */
    public function getMessage();

    /**
     * Set response message.
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get result data.
     *
     * @return string|null
     */
    public function getResult();

    /**
     * Set result data.
     *
     * @param string|null $result
     * @return $this
     */
    public function setResult($result);
}
