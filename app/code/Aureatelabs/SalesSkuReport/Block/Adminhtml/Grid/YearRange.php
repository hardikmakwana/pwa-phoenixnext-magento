<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

namespace Aureatelabs\SalesSkuReport\Block\Adminhtml\Grid;

use Magento\Framework\DataObject;

class YearRange extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    public function render(DataObject $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        if($value == "10-20") {
            return "10 - 20 Year";
        } else if($value == "20-30") {
            return "20 - 30 Year";
        } else if($value == "30-40") {
            return "30 - 40 Year";
        } else if($value == "40-50") {
            return "40 - 50 Year";
        } else if($value == "50-60") {
            return "50 - 60 Year";
        } else if($value == "60-70") {
            return "60 - 70 Year";
        } else if($value == "70-80") {
            return "70 - 80 Year";
        } else if($value == "80-90") {
            return "80 - 90 Year";
        } else {
            return "All Ages Customers";
        }
    }
}
