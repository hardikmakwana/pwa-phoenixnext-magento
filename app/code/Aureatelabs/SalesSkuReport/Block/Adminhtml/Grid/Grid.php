<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

namespace Aureatelabs\SalesSkuReport\Block\Adminhtml\Grid;

use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use Magento\Framework\Registry;
use Magento\Framework\ObjectManagerInterface;
//use Aureatelabs\SalesSkuReport\Model\ResourceModel\Skusales\CollectionFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\CollectionFactory;

class Grid extends Extended {

    protected $registry;

    protected $collectionFactory;

    protected $_objectManager = null;

    protected $resourceConnection;

    public function __construct(
        Context $context,
        Data $backendHelper,
        Registry $registry,
        ObjectManagerInterface $objectManager,
        CollectionFactory $collectionFactory,
        ResourceConnection $resourceConnection,
        array $data = []
    ) {
        $this->_objectManager = $objectManager;
        $this->collectionFactory = $collectionFactory;
        $this->registry = $registry;
        $this->resourceConnection = $resourceConnection;
        parent::__construct($context, $backendHelper, $data);
        $this->setFilterVisibility(false);
    }
    
    protected function _construct() {
        parent::_construct();
    }

    protected function _prepareColumns() {
        $this->addColumn(
            'age',
            [
                'header' => __('Customer Age'),
                'type' => 'text',
                'index' => 'age',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'filter' => false,
                'sortable'  => false
            ]
        );

        $this->addColumn(
            'gender',
            [
                'header' => __('Gender'),
                'type' => 'text',
                'index' => 'gender',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'filter' => false,
                'renderer' => 'Aureatelabs\SalesSkuReport\Block\Adminhtml\Grid\Gender',
                'sortable'  => false
            ]
        );
        $this->addColumn(
            'qty',
            [
                'header' => __('Qty'),
                'type' => 'number',
                'index' => 'qty',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'filter' => false,
                'sortable'  => false
            ]
        );

        $this->addExportType('*/*/exportSalesCsv', __('CSV'));

        return parent::_prepareColumns();
    }

    protected function _prepareCollection()
    {
        $collection = $this->collectionFactory->create();

        if($_GET)
        {
            $sku = $_GET['sku'];
            $fromDate = $_GET['from_date'];
            $toDate = $_GET['to_date'];

            $startDate = date("Y-m-d h:i:s",strtotime($fromDate));
            $endDate = date("Y-m-d h:i:s", strtotime($toDate));

            $query = "SELECT `sku`,COUNT(*) as qty,customer_gender as gender,customer_dob as age FROM `aureate_sku_sales`  WHERE (`order_date` >= '". $startDate ."') AND (`order_date` <= '". $endDate ."') AND (`sku` = '". $sku ."') GROUP BY sku,customer_gender,customer_dob HAVING COUNT(*) > 1 ORDER BY age LIMIT 20";
            $results = $this->resourceConnection->getConnection()->fetchAll($query);    
        } else {
            $query = "SELECT * FROM `aureate_sku_sales` WHERE sku = ''";
            $results = $this->resourceConnection->getConnection()->fetchAll($query);
        }
        

        foreach ($results as $item) {
            if($item['gender'] == 1 || $item['gender'] == 2) {
                $varienObject = new \Magento\Framework\DataObject();
                $varienObject->setData($item);
                $collection->addItem($varienObject);
            }
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareLayout()
    {
        $this->unsetChild('reset_filter_button');
        $this->unsetChild('search_button');

        return parent::_prepareLayout();
    }

}