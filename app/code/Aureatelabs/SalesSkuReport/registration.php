<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Aureatelabs_SalesSkuReport',
    __DIR__
);
