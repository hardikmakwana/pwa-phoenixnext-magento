<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_FreeGifts
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\FreeGifts\Plugin\QuoteApi;

use Magento\Quote\Api\Data\CartItemInterface;
use Magento\Quote\Api\GuestCartItemRepositoryInterface;

/**
 * Class GuestCartItem
 * @package Mageplaza\FreeGifts\Plugin\QuoteApi
 */
class GuestCartItem extends AbstractCartItem
{
    /**
     * @param GuestCartItemRepositoryInterface $subject
     * @param CartItemInterface[] $result
     *
     * @return CartItemInterface[] Array of items.
     */
    public function afterGetList(GuestCartItemRepositoryInterface $subject, $result)
    {
        if (!$this->isEnabled()) {
            return $result;
        }

        $itemFactory = $this->_itemFactory->create();
        foreach ($result as $item) {
            $quoteItem = $itemFactory->load($item->getItemId());
            $data      = $this->getFreeGiftData($quoteItem);
            $extAttr   = $item->getExtensionAttributes();
            if ($extAttr !== null && count($data)) {
                $extAttr->setMpFreeGifts($data['mp_free_gifts'])->setMpFreeGiftsMessage($data['mp_free_gifts_message']);
            }
        }

        return $result;
    }
}
