<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rewards
 * @version   3.0.7
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */


namespace Mirasvit\Rewards\Helper\Balance\Spend;

use Mirasvit\Rewards\Api\Config\Rule\SpendingStyleInterface;
use Mirasvit\Rewards\Helper\Calculation;

class QuoteSubtotalCalc
{

    /**
     * @var float
     */
    private $shippingAmount;
    /**
     * @var \Mirasvit\Rewards\Model\Config
     */
    private $config;
    /**
     * @var \Magento\Framework\Module\Manager
     */
    private $moduleManager;
    /**
     * @var \Mirasvit\Rewards\Service\ShippingService
     */
    private $shippingService;
    /**
     * @var ChargesCalc
     */
    private $chargesCalc;

    public function __construct(
        \Mirasvit\Rewards\Model\Config $config,
        \Mirasvit\Rewards\Service\ShippingService $shippingService,
        ChargesCalc $chargesCalc,
        \Magento\Framework\Module\Manager $moduleManager
    ) {
        $this->config          = $config;
        $this->moduleManager   = $moduleManager;
        $this->shippingService = $shippingService;
        $this->chargesCalc     = $chargesCalc;
    }

    /**
     * @return float
     */
    public function getShippingAmount()
    {
        return $this->shippingAmount;
    }

    /**
     * @param float $shippingAmount
     *
     * @return $this
     */
    public function setShippingAmount($shippingAmount)
    {
        $this->shippingAmount = $shippingAmount;

        return $this;
    }

    /**
     * Get quote subtotal depends on rewards tax settings
     * @param \Magento\Quote\Model\Quote               $quote
     * @param \Magento\Quote\Model\Quote\Address\Total $totals
     *
     * @return float
     */
    public function getQuoteSubtotal($quote, $totals)
    {
        if ($totals) {
            if ($this->config->getGeneralIsIncludeTaxSpending()) {
                $subtotal = $totals->getBaseGrandTotal();
            } else {
                $subtotal = $totals->getBaseSubtotalWithDiscount();
            }
            if (!$this->config->getGeneralIsSpendShipping() && !$quote->isVirtual()) {
                $subtotal -= $totals->getBaseTotalAmount('shipping');
            }
        } else {
            if ($this->config->getGeneralIsIncludeTaxSpending()) {
                $subtotal = $quote->getBaseGrandTotal();
            } else {
                $subtotal = $quote->getBaseSubtotalWithDiscount();
            }
            $subtotal += $this->chargesCalc->getShippingAmount($quote);
            $subtotal = $this->chargesCalc->applyAdditionalCharges($quote, $subtotal);
            $subtotal += $quote->getBaseItemsRewardsDiscount();
        }

        return $subtotal;
    }
}