<?php 
namespace Rokanthemes\CategoryProductTab\Controller\Helper;
use \Psr\Log\LoggerInterface;

class GetCategoryProductTab 
{
	protected $logger;
    protected $categoryFactory;

    public function __construct(LoggerInterface $logger,
       \Magento\Framework\App\Action\Context $context,
       \Magento\Catalog\Model\CategoryFactory $categoryFactory
       ) 
    {
        $this->logger = $logger;
        $this->categoryFactory = $categoryFactory;
    }

    public function getProducts($id)
    {
        $productsCol = $this
        ->getCategory($id)
        ->getProductCollection()
        ->addAttributeToSelect('*')
        ->setPageSize(3);

        $productsCol->getSelect()->orderRand();

        $products = $productsCol->getItems();
        /*$this->_eventManager->dispatch(
            'catalog_block_product_list_collection',
            ['collection' => $products]
        );*/

        $this->logger->info("Rokanthemes\CategoryProductTab get product");

        return $products;
    }

    public function getCategory($id)
    {
        $category = $this->categoryFactory->create()->load($id);
        return $category;
    }
}