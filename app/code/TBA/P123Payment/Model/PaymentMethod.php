<?php

/*
 * Created by 2C2P
 * Date 19 June 2017
 * PaymentMethod is base class / entry point for 2c2p plugin.
 */

namespace TBA\P123Payment\Model;

class PaymentMethod extends \Magento\Payment\Model\Method\AbstractMethod
{
	protected $_code = 'p123payment';
	protected $_isInitializeNeeded = true;
    protected $_canUseInternal = true;
    protected $_canUseCheckout = true;
    protected $_canUseForMultishipping = false;

    //Set additional data and session object and use it further process.
	public function assignData(\Magento\Framework\DataObject $data)
	{
		parent::assignData($data);

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$checkoutSession = $objectManager->create('\Magento\Catalog\Model\Session');

		if(isset($data)) {
			if(!empty($data->getData()['additional_data'])) {
                $checkoutSession->setAgentCodeValue($data->getData()['additional_data']['agentCode']);
			}
		}

		return $this;
  }

    /**
     * Instantiate state and set it to state object
     * @param string $paymentAction
     * @param Varien_Object
     */
    public function initialize($paymentAction, $stateObject)
    {
        $stateObject->setState("Pending_2C2P");
        $stateObject->setStatus("Pending_2C2P");
        $stateObject->setIsNotified(false);
    }
}
