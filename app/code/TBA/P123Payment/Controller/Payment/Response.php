<?php
/*
 * Created by 2C2P
 * Date 20 June 2017
 * This Response action method is responsible for handle the 2c2p payment gateway response.
 */

namespace TBA\P123Payment\Controller\Payment;

class Response extends \P2c2p\P2c2pPayment\Controller\AbstractCheckoutApiAction
{

  	protected $objectManager;

	public function execute()
	{
	    $rc_date = date('Y-m-d h:i:s');
	    $reponseTime = time();
	    $reponsefilename = '/var/www/html/2c2p/backend_'.$reponseTime.'.txt';

		$loggerMessage = $rc_date." : 2c2p Receiving Strating.";
 		file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
		if(!array_key_exists('paymentResponse',$_REQUEST)){
			echo "";

			$loggerMessage = $rc_date." : Can't receive data from 2c2p&123 Service.";
 			file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
			return;
		}
		$result = $_REQUEST["paymentResponse"]; 
		$response = $this->parse123Response($result);

		$xmlResponse=simplexml_load_string($response);
		$post = array();
		foreach ($xmlResponse as $index => $node)
	    {
	        $post[$index] = (String)$node;
			$loggerMessage = $rc_date." : 2c2p Receiving [" . $index . "][".(String)$node."].";
 			file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
	    }
	    
		$post = $this->parse2c2pResponse($post);
		
		$hashHelper   = $this->getHashHelper();
		$configHelper = $this->getConfigSettings();
		//$objCustomerData = $this->getCustomerSession();
		//$isValidHash  = $hashHelper->isValidHashValue($post,$configHelper['secretKey']);
		$secretKey = $configHelper['secretKey'];

		//Get Payment getway response to variable.
		$payment_status_code = $post['respCode'];
		$transaction_ref 	 = $post['tranRef'];
		$approval_code   	 = $post['approvalCode'];
		$payment_status  	 = $post['status'];
		$order_id 		 	 = $post['uniqueTransactionCode'];

		$loggerMessage = $rc_date." : 2c2p Receiving order_id : " . $order_id;
		file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);

		//Get the object of current order.
		
		/*$this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$objectOrder = $this->objectManager->get('\Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
		$order = $this->objectManager->create('Magento\Sales\Model\Order')->load($order_id);*/

		$order = $this->getOrderDetailByOrderId($order_id);

		$isValidHash  = $hashHelper->isValidHashValue2($post,$secretKey);

		//Check whether hash value is valid or not If not valid then redirect to home page when hash value is wrong.
		if(!$isValidHash) {

			if ($order->getId()){
				$order->setState(\Magento\Sales\Model\Order::STATUS_FRAUD);
				$order->setStatus(\Magento\Sales\Model\Order::STATUS_FRAUD);
				$order->save();
			}
			$loggerMessage = $rc_date." : inValidHash.";
 			file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
			//return;
		}

		$metaDataHelper = $this->getMetaDataHelper();
		$metaDataHelper->savePaymentGetawayResponse($post,$order->getCustomerId());

		//check payment status according to payment response.
		if(strcasecmp($payment_status_code, "000") == 0 || strcasecmp($payment_status_code, "00") == 0 ) {
			//IF payment status code is success

			if(!empty($order->getCustomerId()) && !empty($_REQUEST['stored_card_unique_id'])) {
				$intCustomerId = $order->getCustomerId();
				$boolIsFound = false;

				// Fetch data from database by using the customer ID.
				$objTokenData = $metaDataHelper->getUserToken($intCustomerId);
				
				$arrayTokenData = array('user_id' => $intCustomerId,
					'stored_card_unique_id' => $_REQUEST['stored_card_unique_id'],
					'masked_pan' => $_REQUEST['masked_pan'],
					'created_time' =>  date("Y-m-d H:i:s"));

				/* 
				   Iterate foreach and check whether token key is present into p2c2p_token table or not.
				   If token key is already present into database then prevent insert entry otherwise insert token entry into database.
				*/				   
				foreach ($objTokenData as $key => $value) {
					if(strcasecmp($value->getData('masked_pan'), $_REQUEST['masked_pan']) == 0 && 
					   strcasecmp($value->getData('stored_card_unique_id'), $_REQUEST['stored_card_unique_id']) == 0) {
						$boolIsFound = true;
						break;
					}
				}

				if(!$boolIsFound) {
					$metaDataHelper->saveUserToken($arrayTokenData);					
				}
			}
			//Set the complete status when payment is completed.

			if ($order->getId()){
				$order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
				$order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
				$order->save();
			}

			$loggerMessage = $rc_date." : Set the complete status when payment is completed.";
 			file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
			return;

		}
		else if(strcasecmp($payment_status_code, "001") == 0) {
			//Set the Pending payment status when payment is pending. like 123 payment type.
			if ($order->getId()){
				$oldStatus = $order->getStatus();
				if($oldStatus == "Packing" || $oldStatus == "Shipped" || $oldStatus == "Complete" || $oldStatus == "Processing" || $oldStatus == "Order_Received")
					return;
				$order->setState("Pending_2C2P");
				$order->setStatus("Pending_2C2P");
				$order->save();
			}

			$orderIds = intval($order_id);
			$loggerMessage = $rc_date." : Set the Pending payment status when payment is pending. ";
 			file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
			return;
		}
		else{
			//If payment status code is cancel/Error/other.
			$loggerMessage = $rc_date." : payment status code is cancel/Error/other.";

			if ($order->getId()){
				$oldStatus = $order->getStatus();
				if($oldStatus == "Packing" || $oldStatus == "Shipped" || $oldStatus == "Complete" || $oldStatus == "Processing" || $oldStatus == "Order_Received")
					return;
				$order->setState(\Magento\Sales\Model\Order::STATE_CANCELED);
				$order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
				$order->save();
			}

 			file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
			$this->executeCancelAction();
			return;
		}

	}
}
