<?php
namespace TBA\PreOrder\Block\Adminhtml\Order\View;

class ViewOrderShippingCondition extends \Magento\Framework\View\Element\Template
{
	public function __construct(\Magento\Framework\View\Element\Template\Context $context)
	{
		parent::__construct($context);
	}

	public function getTrackingNumber()
    {
    	$orderId = $this->getRequest()->getParam('order_id');
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($orderId);


        return '<br><h3>Ship on Ready</h3>'.$order->getData('is_delivery_on_ready');
    }
}