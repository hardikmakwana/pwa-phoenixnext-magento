<?php /**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace TBA\LOBOT\Controller\Page;
use \Psr\Log\LoggerInterface;
use \TBA\LOBOT\Controller\LobotHelper\TbaCombindOrderInWhileHelper;

class TbaCombineOrderInWhile extends \Magento\Framework\App\Action\Action
{
    protected $tbaCombindOrderInWhileHelper;
    protected $logger;

    public function __construct(
      \Magento\Framework\App\Action\Context $context,
      TbaCombindOrderInWhileHelper $tbaCombindOrderInWhileHelper,
      LoggerInterface $logger)
    {
      $this->logger = $logger;
      $this->tbaCombindOrderInWhileHelper = $tbaCombindOrderInWhileHelper;
      parent::__construct($context);
    }

    public function execute()
    {
      $this->tbaCombindOrderInWhileHelper->CombineShipingInterface();
	  }
}