<?php
namespace TBA\LOBOT\Controller\Page;
use \TBA\LOBOT\Controller\LobotHelper\LobotUpdateStockHelper;
use \Psr\Log\LoggerInterface;

class TbaUpdateProductBookTaxClass extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $lobotUpdateStockHelper;
    protected $logger;

    public function __construct(LoggerInterface $logger,
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        LobotUpdateStockHelper $lobotUpdateStockHelper)
    {
       $this->resultJsonFactory = $resultJsonFactory;

       parent::__construct($context);
       $this->logger = $logger;
       $this->lobotUpdateStockHelper = $lobotUpdateStockHelper;
    }

    public function execute()
    {
      $result = $this->resultJsonFactory->create();
      $data = ['message' => 'Hello world!'];
      echo 'TbaUpdateProductBookTaxClass';

      $this->lobotUpdateStockHelper->UpdateTaxClassToBookCategory();
      return $result->setData($data);
    }
}