<?php
/*use Magento\Framework\App\ObjectManager\ObjectManager;
include('C:\xampp\htdocs\KA-P\vendor\magento\framework\ObjectManager');

$objectManager = ObjectManager::getInstance();
$orderCollectionFactory = $_objectManager->get('Magento\Sales\Model\Order');
$orderList = $orderCollectionFactory->getCollection()->addFieldToSelect('*');
echo "Order Found : " . count($orderList);

$context = $_objectManager->get('\Magento\Framework\View\Element\Template\Context');

foreach ($orderList->Orders as $order) 
{
    echo "Order Found <br>";
	echo "Order Id : " . $order->getId() . "<br>";
	echo "Order IncrementId : " . $order->getIncrementId() . "<br>";
	echo "Order Status : " . $order->getStatusLabel() . "<br>";
	echo "Order CreatedAt : " . $order->getCreatedAt() . "<br>";
	echo "Order CustomerName : " . $order->getCustomerName() . "<br>";
}*/
namespace TBA\LOBOT\Controller\Page;

class OrderToLobot extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
	protected $orderHistory;
	protected $customerSession;
	
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
	   \Magento\Sales\Block\Order\History $orderHistory,
	   \Magento\Customer\Model\Session $customerSession,
       \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
	{
	   $this->resultJsonFactory = $resultJsonFactory;
	   $this->orderHistory = $orderHistory;
	   $this->customerSession = $customerSession;
	   
	   parent::__construct($context);
	}
	
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
		if($this->customerSession->isLoggedIn())
		{
			if (!($customerId = $this->customerSession->getCustomer()->getId())) {
				echo "CustomerSession Not Found";
				return false;
			}
			if (!$this->orders) {
				$this->orders = $this->getOrderCollectionFactory()->create($customerId)->addFieldToSelect(
					'*'
				)->addFieldToFilter(
					'status',
					['in' => $this->_orderConfig->getVisibleOnFrontStatuses()]
				)->setOrder(
					'created_at',
					'desc'
				);
			}
			$data = ['order_found' => count($this->orders)];
		}
		else
		{
			echo "Customer is not log-in.";
			return false;
		}
		
		return $result->setData($data);
	} 
}

