<?php

namespace TBA\LOBOT\Controller\LobotHelper;
use \TBA\LOBOT\Controller\LobotHelper\LobotSftpHelper;
use \Psr\Log\LoggerInterface;
//use \TBA\LOBOT\Controller\LobotHelper\LobotConstants;
use \Datetime;

class LobotOrderTsvBuilder {
    protected $logger;
    //protected $lobotConstants;
	
    public function __construct(
        //LobotConstants $lobotConstants,
        LobotSftpHelper $lobotSftpHelper,
        LoggerInterface $logger) {
        $this->logger = $logger;
        $this->lobotSftpHelper = $lobotSftpHelper;
        //$this->lobotConstants = $lobotConstants;
    }
    
    public function OrderToLobotTsvBuilder($orderData)
    {
        $headerData = $orderData['header'];
        $headerResultString = "";

        $headerResultString .=  $headerData['record_indicator']."\t";
        $headerResultString .=  $headerData['r3']."\t";
        $headerResultString .=  $headerData['unique_number']."\t";
        $headerResultString .=  $headerData['order_number']."\t";
        $headerResultString .=  $headerData['order_serial_number']."\t";
        $headerResultString .=  $headerData['order_date']."\t";
        $headerResultString .=  $headerData['order_member_number']."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  $headerData['order_name']."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  str_replace("\n", " ", $headerData['delivery_address'])."\t";
        $headerResultString .=  ''."\t";//str_replace("\n", " ", $headerData['order_order_address2'])."\t";
        $headerResultString .=  ''."\t";//str_replace("\n", " ", $headerData['order_order_address3'])."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  $headerData['order_phone_number']."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  $headerData['destination_name']."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  $headerData['order_delivery_post_code']."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  str_replace("\n", " ", $headerData['delivery_address'])."\t";
        $headerResultString .=  ''."\t";//str_replace("\n", " ", $headerData['delivery_address2'])."\t";
        $headerResultString .=  ''."\t";//str_replace("\n", " ", $headerData['delivery_address3'])."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  $headerData['destination_phone_number']."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  $headerData['name_of_client']."\t";
        $headerResultString .=  $headerData['name_of_client_local']."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  str_replace("\n", " ", $headerData['client_address1'])."\t";
        $headerResultString .=  str_replace("\n", " ", $headerData['client_address2'])."\t";
        $headerResultString .=  str_replace("\n", " ", $headerData['client_address3'])."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  str_replace("\n", " ", $headerData['client_address1_local'])."\t";
        $headerResultString .=  str_replace("\n", " ", $headerData['client_address2_local'])."\t";
        $headerResultString .=  str_replace("\n", " ", $headerData['client_address3_local'])."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  $headerData['requestor_phone_number']."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  '8'."\t";//Payment Method
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  $headerData['shipping_amount']."\t";//Shipping Cost
        $headerResultString .=  '0'."\t";//Cod
        $headerResultString .=  $headerData['reward_spend']."\t";//Point use 
        $headerResultString .=  $headerData['discount_amount']."\t";//Adjust Amount
        $headerResultString .=  '0'."\t";//Point Remain
        $headerResultString .=  "บริษัท คา โด คา วะ อมรินทร์ จํา กัด"."\t";
        $headerResultString .=  $headerData['order_tax']."\t";
        $headerResultString .=  " "."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  " "."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  "เบอร์ โทรศัพท์ 02-434-0333"."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  " "."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  '1'."\t";//Amount Display
        $headerResultString .=  '0'."\t";//Box Shipment
        $headerResultString .=  '0'."\t";//Ship-to Party
        $headerResultString .=  '0'."\t";//Bussiness Office
        $headerResultString .=  ''."\t";
        $headerResultString .=  '01'."\t";//Courier Service
        $headerResultString .=  '0'."\t";//Courier Type
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  '0'."\t";//Classification of me
        $headerResultString .=  '0'."\t";//Precision Machine Segment
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        $headerResultString .=  '011'."\t";//Data Partition
        $headerResultString .=  ''."\t";
        $headerResultString .=  ''."\t";
        
        $detailsData = $orderData['detials'];
        $detailsResultData = array();
        $itemOrder = 0;

        foreach ($detailsData as $detail) {
            $itemOrder++;
            $detailResultString = "";
            $detailResultString .=  $detail['record_indicator']."\t";
            $detailResultString .=  $itemOrder."\t";
            $detailResultString .=  '0'."\t";//$detail['catagories']."\t";
            $detailResultString .=  $detail['item_number']."\t";//R3 Mat
            $detailResultString .=  $detail['product_number']."\t";
            $detailResultString .=  ''."\t";
            $detailResultString .=  $detail['product_name']."\t";
            $detailResultString .=  ''."\t";
            $detailResultString .=  $detail['quantity']."\t";
            $totalPrice = $detail['price'] * $detail['quantity'];
            $detailResultString .=  $totalPrice."\t";//Amount Money
            $detailResultString .=  '0'."\t";//Issue Point
            $detailResultString .=  ''."\t";
            $detailResultString .=  ''."\t";
            $detailResultString .=  ''."\t";
            $detailResultString .=  ''."\t";
            $detailResultString .=  ''."\t";
            $detailResultString .=  ''."\t";
            $detailResultString .=  ''."\t";
            $detailResultString .=  ''."\t";
            $detailResultString .=  ''."\t";
            $detailResultString .=  ''."\t";
            array_push($detailsResultData,$detailResultString);
        }
        
        return $this->BuildTsvContent($headerResultString,$detailsResultData);
    }
    
    private function BuildTsvContent($headerResultString,$detailsResultData)
    {
        $tsvContent = "";
        $tsvContent .= $headerResultString . "\r\n";

        $detialsPods = array();
        $index = 0;
        $maxIndex = count($detailsResultData);
        foreach ($detailsResultData as $detialData)
        {
            //$index++;
            $tsvContent .= $detialData . "\r\n";
            /*if($index == $maxIndex)
            {
                $tsvContent .= $detialData;
            }
            else
            {
                $tsvContent .= $detialData . "\r\n";
            }*/
        }
        
        return $this->BuildTsvFile($tsvContent,true);
    }
    
    public function BuildTsvFile($tsvContent,$isRealOne)
    {
        $dt = new DateTime();
        $timeInformat = $dt->format('YmdHis');
        //$timeInformat = DateTime::createFromFormat('YmdHis', 'now');
        if($isRealOne)
            $fileName = "BHKSKA".$timeInformat.".tsv";
        else
            $fileName = "TBA_BHKSKA".$timeInformat.".tsv";
        //$filePath = $this->lobotConstants->LOCAL_TMP_INTERFACE_DIR . $fileName;

        $filePath = '/var/www/html/tmpLobotInterface/'.$fileName;

        if ( ! file_exists($filePath) ) {
            touch($filePath);
        }
        
        $stringContent = $tsvContent;

        $filePutContents = file_put_contents($filePath, $stringContent, FILE_APPEND);
        if ($filePutContents !== false) 
        {
            $loggerMessage = "File created (" . basename($filePath) . ")";
            $this->logger->info($loggerMessage);
            if($isRealOne)
                $this->lobotSftpHelper->PassFileToLobot($filePath);
        } 
        else 
        {
            $loggerMessage = "Cannot create file (" . $fileName . ")";
            $this->logger->info($loggerMessage);
        }

        return $filePath;
    }
}
