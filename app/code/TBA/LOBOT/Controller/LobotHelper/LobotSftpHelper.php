<?php
namespace TBA\LOBOT\Controller\LobotHelper;
use \Psr\Log\LoggerInterface;
//use \TBA\LOBOT\Controller\LobotHelper\LobotConstants;
use phpseclib\Crypt\RSA;
use phpseclib\Net\SSH2;
use phpseclib\Net\SFTP;


class LobotSftpHelper
{
    protected $logger;
    //protected $lobotConstants;
	
    public function __construct(
        //LobotConstants $lobotConstants,
        LoggerInterface $logger) {
        $this->logger = $logger;
        //$this->lobotConstants = $lobotConstants;
    }
	
    public function GetLobotConfirmOrder(){
        $glob_key = "/^(BHKSKJ)/";
        return $this->GetFilesByGlobeKey($glob_key);
    }

    public function GetLobotShipmentResult(){
        $glob_key = "/^(BHKSKJ)/";
        return $this->GetFilesByGlobeKey($glob_key);
    }

    public function GetLobotDeliveryResult(){
        $glob_key = "/^(BHKHKJ)/";
        return $this->GetFilesByGlobeKey($glob_key);
    }

    public function GetLobotUpdateStock(){
        $glob_key = "/^(BAEZSY)/";
        return $this->GetFilesByGlobeKey($glob_key);
    }

    public function PassFileToLobot($targetFile)
    {
        $remoteDir = '/C:/Users/KDKBLP/inbound/';
        $localDirBin = '/var/www/html/tmpLobotInterface/Bin/';

        $sftp = $this->SFTP_SSH2();

        if($sftp != null){
            $sftp->chdir($remoteDir);

            $uploadFilename = basename($targetFile);

            if($sftp->put($uploadFilename, file_get_contents($targetFile))){
                $loggerMessage = "Upload File : " . $uploadFilename . " successfully.";
                $this->logger->info($loggerMessage);
                $sftp->chmod(0755, $uploadFilename);

                if(rename($targetFile, $localDirBin.$uploadFilename)){
                    $loggerMessage = "Move File " . $uploadFilename . " to ".$localDirBin.".";
                    $this->logger->info($loggerMessage);
                }else{
                    $loggerMessage = "Can't Move File " . $uploadFilename . " to ".$localDirBin.".";
                    $this->logger->error($loggerMessage);
                }

            }else{
                $loggerMessage = "Can't Upload File : " . $uploadFilename . " .";
                $this->logger->error($loggerMessage);
            }

            $sftp->disconnect();
            $loggerMessage = "SFTP Disconnected successfully.";
            $this->logger->info($loggerMessage);
        }
    }

    private function GetFilesByGlobeKey($glob_key)
    {
        $remoteDir = '/C:/Users/KDKBLP/outbound/';
        //$remoteDirOld = '/C:/Users/KDKBLP/outbound/old/';
        $localDir = '/var/www/html/tmpLobotInterface/';
        $sftp = $this->SFTP_SSH2();
        if($sftp != null){
            $sftp->chdir($remoteDir);

            $files = $sftp->nlist();

            $filesNameList = array();

            foreach ($files as $filename) 
            {
                if($sftp->is_file($filename)){
                    $f = basename($filename);
                    $fileSize = $sftp->filesize($filename);
                    //if($fileSize > 0){
                        if (preg_match($glob_key."i", $f)) {
                            $loggerMessage = "Found file " . $f. ". Size: " .$fileSize .".";
                            $this->logger->info($loggerMessage);
                            if($sftp->get($filename, $localDir.$f)){
                                $loggerMessage = "Download File " . $f . " to ".$localDir.".";
                                $this->logger->info($loggerMessage);
                                array_push($filesNameList,$f);
                                if($sftp->delete($filename, false)){
                                    $loggerMessage = "Delete File " . $f . " to ".$remoteDir.".";
                                    $this->logger->info($loggerMessage);
                                }else{
                                    $loggerMessage = "Can't Delete File " . $f . " to ".$remoteDir.".";
                                    $this->logger->error($loggerMessage);
                                }
                            }else{
                                $loggerMessage = "Can't Download File : " . $uploadFilename . " .";
                                $this->logger->error($loggerMessage);
                            }
                        }
                    //}
                }
            }

            $sftp->disconnect();
            $loggerMessage = "SFTP Disconnected successfully.";
            $this->logger->info($loggerMessage);

            return $filesNameList;
        }
    }
    private function SFTP_SSH2(){

        $host = '210.155.96.163';
        $port = '22';

        $pri_key = '/var/www/html/LobotKey/id_rsa';
        $passphrase = 'g7DcYLXU';
        $username = 'KDKBLP';

        $privatekey = new RSA();
        $privatekey_data = file_get_contents($pri_key);

        $loggerMessage = "SFTP_SSH2 after gen pri key " . $privatekey;
        $this->logger->info($loggerMessage);

        $privatekey->setPassword($passphrase);
        if (!$privatekey->loadKey($privatekey_data))
        {
            $loggerMessage = "Can't load key file from " . $pri_key .".";
            $this->logger->error($loggerMessage);
        }

        $loggerMessage = "SFTP_SSH2 after load pri key";
        $this->logger->info($loggerMessage);

        //$sftp = new SSH2($host);
        $sftp = new SFTP($host);

        $loggerMessage = "SFTP_SSH2 after new SFTP( " . $host . " )";
        $this->logger->info($loggerMessage);

        $loggerMessage = "SFTP_SSH2 before login username( " . $username . " )" . " username( " . $privatekey . " )";
        $this->logger->info($loggerMessage);

        if (!$sftp->login($username, $privatekey))
        {
            $loggerMessage = "Can't log in to " . $host .".";
            $this->logger->error($loggerMessage);
        }

        $loggerMessage = "SFTP_SSH2 after login privatekey( " . $privatekey . " )";
        $this->logger->info($loggerMessage);

        if($sftp->isConnected()) {
            $loggerMessage = "Connected to ". $host ." successfully.";
            $this->logger->info($loggerMessage);
        }else{
            $sftp = null;
            $loggerMessage = "Can't connected to ". $host .".";
            $this->logger->error($loggerMessage);
        }

        return $sftp;
    }
}
