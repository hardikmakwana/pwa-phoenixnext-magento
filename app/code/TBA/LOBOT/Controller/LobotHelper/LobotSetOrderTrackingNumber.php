<?php
namespace TBA\LOBOT\Controller\LobotHelper;
use \Psr\Log\LoggerInterface;

class LobotSetOrderTrackingNumber
{
    protected $logger;
    protected $orderFactory;
    public function __construct(
        LoggerInterface $logger,
       \Magento\Sales\Model\OrderFactory $orderFactory) 
    {
        $this->logger = $logger;
        $this->orderFactory = $orderFactory;
    }

    public function UpdateTrackingNumberByOrderId($shipedOrderList)
    {
        foreach ($shipedOrderList as $orderId => $dataBean) 
        {
            $trackingNumber = $dataBean['trackingNumber'];
            $shippingDate = $dataBean['shippingDate'];

            $order = $this->orderFactory->create()->loadByIncrementId($orderId);
            if ($order->getId()){
                $order->setData('trackingNumber',$trackingNumber);
                $order->setData('shippingDate',$shippingDate);
                $order->setState(\Magento\Sales\Model\Order::STATE_COMPLETE);
                $order->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETE);
                $order->save();

                $this->logger->info("LobotSetOrderTrackingNumber complete Order : " . $orderId . " : with TN : " . $order->getData('trackingNumber'));
                $this->logger->info("LobotSetOrderTrackingNumber complete Order : " . $orderId . " : with SD : " . $order->getData('shippingDate'));
            }
            else
            {
                $this->logger->info("LobotSetOrderTrackingNumber Not Found Order : " . $orderId);
            }
            
        }
    }
}